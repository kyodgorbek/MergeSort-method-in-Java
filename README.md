# MergeSort-method-in-Java



import java.util.*;

public class MergeSort {
    public static void main(String[] args){
     int[] list = {14, 32, 67, 76, 23, 41, 58, 85};
     System.out.println("before: " + Arrays.toString(list));
      merge.Sort(list);
      System.out.println("after : " + Arrays.toString(list));
   }
   
     // Places the elements of the given array into sorted array order
     // using the merge sort algorithm
     //post: array is in sorted (nondecrising) order
     public static void mergeSort(int[] a) {
     if(a.length > 1) {
      int left = Arrays.copyofRange(a, 0, a.length / 2);
      int left = Arrays.copyofRange(a, a.length / 2, a.length);
      //recursively sort th two halves
       
      mergeSort(left);
      mergeSort(right);
      
      // merge the sorted halves into a sorted whole
      merge(a, left, right);
   
     }
   }
    
    // Merges the given left and right array into the given
    // result array.
    // pre : result is empty; left/rigth are sorted
    // post: result contains result  of merging sorted lists
    public static void merge(int[] result, int[] left, int[] right) {
     int i1 = 0;
     int i2 = 0;
     for (int i = 0; i < result.length; i++){
       if (i >= right.length ||  (i1 < left.length && left[i1] <= right[i2])) {
             result[i] = left[i1];   // take from left
             i1++;
        } else {
         result[i] = right[i2]; // take from right
         i2++;
       }
      }
     }
    }           
                                       
